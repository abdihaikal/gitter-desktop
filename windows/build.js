'use strict';

const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
const { argv } = require('yargs');
const Promise = require('bluebird');

const nwappManifest = require('../nwapp/package.json');

const appVersion = nwappManifest.version;

const stat = Promise.promisify(fs.stat);

const possibleProgramFilePaths = ['C:\\Program Files (x86)', 'C:\\Program Files'];

const possibleSignToolPaths = [
  'Windows Kits\\10\\bin\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\x86\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.17763.0\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.17763.0\\x86\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.17134.0\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.17134.0\\x86\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.16299.0\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.16299.0\\x86\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.15063.0\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.15063.0\\x86\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.14393.0\\x64\\signtool.exe',
  'Windows Kits\\10\\bin\\10.0.14393.0\\x86\\signtool.exe',
  'Windows Kits\\8.1\\bin\\x64\\signtool.exe',
  'Windows Kits\\8.1\\bin\\x86\\signtool.exe',
  'Windows Kits\\8.0\\bin\\x64\\signtool.exe',
  'Windows Kits\\8.0\\bin\\x86\\signtool.exe',
  'Microsoft SDKs\\Windows\\v7.1\\Bin\\signtool.exe',
  'Microsoft SDKs\\Windows\\v7.1A\\Bin\\signtool.exe',
];

const resultantStatResults = possibleProgramFilePaths.reduce(function (
  statResults,
  programFilePath,
) {
  return statResults.concat(
    possibleSignToolPaths.map(function (signToolPath) {
      const fullSignToolPath = path.join(programFilePath, signToolPath);
      return stat(fullSignToolPath).then(function () {
        return fullSignToolPath;
      });
    }),
  );
},
[]);

Promise.any(resultantStatResults)
  .then(function (signToolPath) {
    console.log('signToolPath', signToolPath);
    const certPassword = argv.password || argv.p || '';

    const commandList = [
      `"${signToolPath}" sign /debug /f "${path.resolve(
        __dirname,
        '..\\certificates\\troupe-cert.pfx',
      )}" /p "${certPassword}" "${path.resolve(__dirname, '..\\opt\\Gitter\\win32\\Gitter.exe')}"`,
      `"C:\\Program Files (x86)\\Inno Setup 5\\ISCC.exe" "${path.resolve(
        __dirname,
        'gitter.iss',
      )}"`,
      `"${signToolPath}" sign /debug /f "${path.resolve(
        __dirname,
        '..\\certificates\\troupe-cert.pfx',
      )}" /p "${certPassword}" "${path.resolve(__dirname, '..\\artefacts\\GitterSetup*')}"`,
      `rename "${path.resolve(
        __dirname,
        '..\\artefacts\\GitterSetup.exe',
      )}" "GitterSetup-${appVersion}.exe"`,
    ];

    const commandRunner = function (command) {
      return new Promise(function (resolve) {
        const child = exec(command, function (err, stdout, stderr) {
          resolve({
            command,
            stdout,
            stderr,
            error: err,
          });
        });

        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
      });
    };

    // Run the commands in series
    const commandChain = commandList.reduce(function (seriesCommandChain, command) {
      return seriesCommandChain.then(function (commandResult) {
        console.log('commandResult', commandResult);
        if (commandResult.command) {
          console.log(`Done: ${commandResult.command}`);
          console.log('--------------------------------------------');
        }
        if (commandResult.error) {
          console.log(commandResult.stderr);
          throw commandResult.error;
        }

        // Keep the chain going
        console.log(`> ${command}`);
        return commandRunner(command);
      });
    }, Promise.resolve({}));

    return commandChain.catch(function (err) {
      console.log('err', err, err.stack);
    });
  })
  .catch(Promise.AggregateError, function (err) {
    // ignore any failed checks
    console.log(
      'Probably could not find the `signtool.exe`, try installing the Microsoft SDK and check that we are looking in the right place: open this file and look at `possibleSignToolPaths`',
      err,
    );
  });
