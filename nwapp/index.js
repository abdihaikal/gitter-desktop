'use strict';

const log = require('loglevel');

log.setDefaultLevel('debug');

const urlJoin = require('url-join');
const AutoLaunch = require('auto-launch');
const Gitter = require('gitter-realtime-client');

const pkg = require('./package.json');
const config = require('./utils/config');

log.setLevel(config.logLevel);
const preferences = require('./utils/user-preferences');
const CLIENT = require('./utils/client-type');
const notifier = require('./utils/notifier');
const events = require('./utils/custom-events');
const quitApp = require('./utils/quit-app');
const pollForUpdates = require('./utils/update/poll-for-updates');
const flushCookies = require('./utils/flush-cookies');

// components
const generateMenuItems = require('./components/menu-items');
const CustomTray = require('./components/tray');
const CustomMenu = require('./components/menu');
const TrayMenu = require('./components/tray-menu');

const LoginView = require('./lib/login-view');

let win;
let mainWindow; // this is the chat window (logged in)
let loginView; // log in form
const state = {
  updateAvailable: false,
};

const autoLauncher = new AutoLaunch({
  name: 'Gitter',
});

function addAuthHeadersMiddleware(details) {
  details.requestHeaders.push({
    name: 'Authorization',
    value: `Bearer ${preferences.token}`,
  });

  return {
    requestHeaders: details.requestHeaders,
  };
}

function showAuth() {
  log.trace(`showAuth, loginView exists?${Boolean(loginView)}`);
  if (loginView) return;

  loginView = new LoginView();

  loginView.on('accessTokenReceived', function (accessToken) {
    log.trace(`accessTokenReceived${accessToken}`);
    if (loginView) {
      preferences.token = accessToken;
      // eslint-disable-next-line no-use-before-define
      initApp(accessToken);
      loginView.destroy();
    }
  });

  loginView.on('destroy', function () {
    loginView = null;
  });
}

function signout() {
  log.trace('signout() started');

  // Remove the middleware so we don't try to pass a `null` token which
  // would cause us to be in a `/login` redirect loop
  chrome.webRequest.onBeforeSendHeaders.removeListener(addAuthHeadersMiddleware);

  flushCookies(win)
    .then(function () {
      preferences.token = null;

      // only close the window if we can, otherwise app may crash
      if (mainWindow) {
        mainWindow.close(true);
      }

      showAuth();

      events.emit('user:signedOut');
      log.trace('signout() complete!');

      return null;
    })
    .catch((err) => {
      log.error('Something went wrong while flushing cookies', err);
    });
}

/**
 * showLoggedInWindow() handles the logic for displaying loggedin.html
 *
 * exec   - String, code to be evaluated once the iFrame has loaded.
 * @void
 */
function showLoggedInWindow(exec) {
  log.trace('showLoggedInWindow()');

  nw.Window.open(
    'loggedin.html',
    {
      // We use the same ID so windows open in the same place
      id: 'gitterLoggedInWindow',
      focus: true,
      width: 1200,
      height: 800,
    },
    (openedWindow) => {
      mainWindow = openedWindow;
      const iframeTarget = mainWindow.window.document.getElementById('mainframe');

      const menu = new CustomMenu({
        label: 'Gitter',
        generateMenuItems: () => generateMenuItems(state),
        filter(item) {
          if (item.support && item.support.indexOf(CLIENT) < 0) {
            return false;
          }

          if (item.auth) {
            return item.auth && Boolean(preferences.token);
          }

          return true;
        },
      });

      mainWindow.menu = menu.get();

      // FIXME: temporary fix, needs to be repainted
      events.on('menu:updated', function () {
        log.trace('menu:updated re-painting menu in mainwindow', mainWindow);
        if (mainWindow) {
          mainWindow.menu = null;
          mainWindow.menu = menu.get();
        }
      });

      mainWindow.on('loaded', function () {
        if (exec) {
          mainWindow.eval(iframeTarget, exec);
        }
      });

      mainWindow.on('closed', function () {
        log.trace('mainWindow:closed');
        mainWindow = null;
      });

      mainWindow.on('focus', function () {
        log.trace('mainWindow:focus');
        // TODO: Remove this hack
        const toExec =
          "var cf = document.getElementById('content-frame'); if (cf) cf.contentWindow.dispatchEvent(new Event('focus'));";
        mainWindow.eval(iframeTarget, toExec);
      });

      mainWindow.on('blur', function () {
        log.trace('mainWindow:blur');
        // TODO: Remove this hack
        const toExec =
          "var cf = document.getElementById('content-frame'); if (cf) cf.contentWindow.dispatchEvent(new Event('blur'));";
        mainWindow.eval(iframeTarget, toExec);
      });

      mainWindow.on('new-win-policy', function (frame, url, policy) {
        nw.Shell.openExternal(url);
        policy.ignore();
      });
    },
  );
}

// reopen window when they are all closed
function reopen() {
  if (!mainWindow) {
    if (!preferences.token || preferences.token.length === 0) {
      return showAuth();
    }
    return showLoggedInWindow();
  }
}

function showMainWindowDeveloperTools() {
  log.trace('showMainWindowDeveloperTools()');

  mainWindow.showDevTools();
}

function showBackgroundDeveloperTools() {
  log.trace('showBackgroundDeveloperTools()');

  // Show dev tools for the background page
  // via https://github.com/nwjs/nw.js/issues/4578#issuecomment-222745051
  chrome.developerPrivate.openDevTools({
    renderViewId: -1,
    renderProcessId: -1,
    extensionId: chrome.runtime.id,
  });
}

function navigateWindowTo(uri) {
  const toExec = `window.gitterLoader('${uri}');`;

  if (!mainWindow) {
    // load window and then navigate
    return showLoggedInWindow(toExec);
  }

  // simply navigate as we have window
  const iframeTarget = mainWindow.window.document.getElementById('mainframe');
  mainWindow.eval(iframeTarget, toExec);
  mainWindow.focus();
  mainWindow.show();
}

function setupTray(targetWindow) {
  const customTray = new CustomTray();
  targetWindow.tray = customTray.get();

  const roomMenu = new TrayMenu();
  targetWindow.tray.menu = roomMenu.get();

  // FIXME: temporary fix, needs to be repainted
  events.on('traymenu:updated', function () {
    targetWindow.tray.menu = roomMenu.get();
  });

  // Set unread badge
  events.on('traymenu:unread', function (unreadCount) {
    targetWindow.setBadgeLabel(unreadCount.toString());
  });

  // Remove badge
  events.on('traymenu:read', function () {
    targetWindow.setBadgeLabel('');
  });

  if (CLIENT !== 'osx') {
    targetWindow.tray.on('click', reopen);
  }
}

// initialises and adds listeners to the top level components of the UI
function initGUI() {
  log.trace('initGUI()');
  win = nw.Window.get();

  // Set up tray(OSX)/menu bar(Windows)
  if (preferences.showInMacMenuBar) {
    setupTray(win);
  }

  events.on('preferences:change:showInMacMenuBar', function (newValue) {
    if (newValue) {
      setupTray(win);
    } else if (win.tray) {
      win.tray.remove();
    }
  });

  nw.App.on('reopen', reopen); // When someone clicks on the dock icon, show window again.

  win.on('close', function (evt) {
    log.trace('win:close');
    if (evt === 'quit') {
      quitApp();
    } else {
      this.close(true);
    }
  });
}

// establishes faye connection and manages signing in/out flow
function initApp() {
  const { token } = preferences;
  const hasToken = token && token.length > 0;
  log.trace('initApp -> hasToken?', hasToken, token);

  if (hasToken) {
    // Be sure to remove this later on when we sign out (see signout())
    chrome.webRequest.onBeforeSendHeaders.addListener(
      addAuthHeadersMiddleware,
      {
        urls: [urlJoin(config.baseUrl, '/*'), 'https://api.gitter.im/*'],
      },
      ['blocking', 'requestHeaders'],
    );
  }
  // user not logged in
  else {
    showAuth();
    return;
  }

  let terminating = false;
  const accessTokenFailureExtension = {
    incoming: Gitter.wrapExtension(function (message, callback) {
      if (message.error && message.advice && message.advice.reconnect === 'none') {
        // advice.reconnect == 'none': the server has effectively told us to go away for good
        if (!terminating) {
          terminating = true;
          // More needs to be done here!
          log.error(
            'Access denied. Realtime communications with the server have been disconnected.',
            message,
          );

          window.alert('Realtime communications with the server have been disconnected.');
          signout();
        }
      }

      callback(message);
    }),
  };

  // Realtime client to keep track of the user rooms.
  let client = new Gitter.RealtimeClient({
    fayeUrl: config.fayeUrl,
    authProvider(cb) {
      return cb({ token, client: CLIENT });
    },
    extensions: [accessTokenFailureExtension],
  });

  events.emit('user:signedIn');
  events.on('traymenu:clicked', navigateWindowTo);
  events.on('traymenu:signout', signout);
  events.on('menu:signout', signout);

  events.on('menu:toggle:next', function () {
    if (!mainWindow) return;
    const iframeTarget = mainWindow.window.document.getElementById('mainframe');
    const isNextActive =
      mainWindow.eval(iframeTarget, "document.cookie.indexOf('gitter_staging=staged');") >= 0;
    log.trace(`toggle:next is next currently active? ${isNextActive}`);
    if (isNextActive) {
      log.trace('toggle:next Going to production');
      preferences.next = false;
      mainWindow.eval(
        iframeTarget,
        "document.cookie='gitter_staging=none;domain=.gitter.im;path=/;expires=' + new Date(Date.now() + 31536000000).toUTCString(); location.reload();",
      );
    } else {
      log.trace('toggle:next Going to next/staging');
      preferences.next = true;
      mainWindow.eval(
        iframeTarget,
        "document.cookie='gitter_staging=staged;domain=.gitter.im;path=/;expires=' + new Date(Date.now() + 31536000000).toUTCString(); location.reload();",
      );
    }
  });

  events.on('menu:toggle:background-devtools', showBackgroundDeveloperTools);
  events.on('menu:toggle:mainwindow-devtools', showMainWindowDeveloperTools);

  events.on('user:signedOut', function () {
    client.disconnect();
    client = null;
  });

  events.on('preferences:change:launchOnStartup', function (newValue) {
    autoLauncher[newValue ? 'enable' : 'disable'](function (err) {
      if (err) throw err;
    });
  });

  const rooms = new Gitter.RoomCollection([], { client, listen: true });

  client.on('change:userId', function (userId) {
    events.emit('realtime:connected', rooms);
    log.trace('realtime connected()');

    if (!client) return;
    log.trace('attempting to subscribe()');
    client.subscribe(`/v1/user/${userId}`, function (msg) {
      if (msg.notification === 'user_notification') {
        log.info(`Showing user_notification: ${msg.title}`);
        notifier({
          title: msg.title,
          message: msg.text,
          icon: msg.icon,
          click() {
            log.info('Notification user_notification clicked. Moving to', msg.link);
            navigateWindowTo(msg.link);
          },
        });
      } else if (msg.error && msg.advice && msg.advice.reconnect === 'none') {
        log.info('Realtime communications with the server have been disconnected. Signing out...');
        signout();
      }
    });
  });

  if (preferences.launchHidden !== true) {
    showLoggedInWindow();
  }
}

process.on('uncaughtException', function (err) {
  log.error(`Caught exception: ${err}`, { exception: err });
  log.error(err.stack);

  if (preferences.showNotificationsForDesktopErrors) {
    notifier({
      title: 'Uncaught error in app (open devtools)',
      message: err,
      click() {
        showBackgroundDeveloperTools();
      },
    });
  }
});

(function () {
  log.info('execPath:', process.execPath);
  log.info('version:', pkg.version);

  pollForUpdates();
  events.on('update:available', () => {
    state.updateAvailable = true;
  });

  initGUI(); // intialises menu and tray, setting up event listeners for both
  initApp();

  autoLauncher.isEnabled(function (enabled) {
    if (enabled !== preferences.launchOnStartup) {
      autoLauncher[preferences.launchOnStartup ? 'enable' : 'disable'](function (err) {
        if (err) {
          throw err;
        }
      });
    }
  });
})();
